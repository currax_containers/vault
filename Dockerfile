# base image
FROM centos:latest

# set vault version
ENV VAULT_VERSION 0.10.3

# create a new directory
RUN mkdir -p /vault-config
RUN mkdir -p /vault/{data,logs,policies}

# download dependencies
RUN yum install -y ca-certificates wget unzip

# download and set up vault
RUN wget --quiet --output-document=/tmp/vault.zip https://releases.hashicorp.com/vault/${VAULT_VERSION}/vault_${VAULT_VERSION}_linux_amd64.zip && \
    unzip /tmp/vault.zip -d /usr/local/bin && \
    rm -f /tmp/vault.zip && \
    chmod +x /usr/local/bin/vault

# update PATH
ENV PATH="PATH=$PATH:$PWD/vault"

# add the default config file
COPY default-config/vault-config.json /vault-config/vault-config.json

# expose port 8200
EXPOSE 8200

# run vault
CMD ["/usr/local/bin/vault","server","-config=/vault-config/vault-config.json"]